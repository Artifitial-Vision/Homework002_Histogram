import cv2
from matplotlib import pyplot
class Histo01:

    @staticmethod
    def histogram():
        img = cv2.imread('me-262.jpg')
        altura, anchura, canales = img.shape
        for al in range(altura):
            for an in range(anchura):
                x = (img[al, an])
                img[al, an] = sum(img[al, an]) / 3
                z = (img[al, an])
        # hist = cv2.calcHist([img],[0],None,[256],[0,256])
        pyplot.hist(img.ravel(), 256, [0, 256]);
        pyplot.show()
Histo01.histogram()
